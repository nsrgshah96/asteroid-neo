//
//  HomeVC.swift
//  Asteroid NEO
//
//  Created by Nisarg Shah on 31/10/22.
//

import UIKit
import SwiftyJSON
import Accelerate
import Charts

struct NEODataObjects {
    var date: Date
    var numbers: Int
}

class NearObjectChartVC: UIViewController {
    
    @IBOutlet private weak var lblFastestAsteroid: UILabel!
    @IBOutlet private weak var lblClosestAsteroid: UILabel!
    @IBOutlet private weak var lblAsteroidsAvgSize: UILabel!
    @IBOutlet private weak var vwChart: BarChartView!

    var dateRange = (Date(),Date())
    private var neoData: JSON? {
        didSet {
            DispatchQueue.main.async {
                self.loadChartData()
                self.findFastest()
                self.findClosest()
                self.findAverageSize()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAsteroidData(start: dateRange.0, end: dateRange.1)
    }
    
    private func loadChartData() {
        guard var arrData = neoData?.dictionaryValue.compactMap({ (key,value) in
            if let date = key.toDate() {
                return NEODataObjects(date: date, numbers: value.arrayValue.count)
            } else {
                return nil
            }
        }) else { return }
        
        arrData.sort(by: { $0.date < $1.date })
        
        let entries = arrData.enumerated().compactMap({ BarChartDataEntry(x: Double($0.offset), y: Double($0.element.numbers)) })
        
        let arrDataSet = BarChartDataSet(entries: entries)
        arrDataSet.colors = [.systemMint]
        arrDataSet.drawValuesEnabled = true
        arrDataSet.valueTextColor = .black
        arrDataSet.valueFont = .systemFont(ofSize: 18)
        
        let xAxis = vwChart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelTextColor = .black
        xAxis.labelFont = .systemFont(ofSize: 13)
        xAxis.drawAxisLineEnabled = true
        xAxis.axisLineColor = .black
        xAxis.drawGridLinesEnabled = false
        xAxis.drawLabelsEnabled = true
        xAxis.labelRotationAngle = -45
        xAxis.granularityEnabled = true
        xAxis.granularity = 1
        xAxis.valueFormatter = CustomDateAxisFormatter(arrData: arrData.map({ $0.date.string(withFormat: .dateMonthYear) }))
        
        let rightAxis = vwChart.rightAxis
        rightAxis.drawGridLinesEnabled = false
        rightAxis.drawAxisLineEnabled = false
        rightAxis.drawLabelsEnabled = false
        
        let leftAxis = vwChart.leftAxis
        leftAxis.drawGridLinesEnabled = false
        leftAxis.drawAxisLineEnabled = false
        leftAxis.drawLabelsEnabled = false
        leftAxis.axisMinimum = 0
        leftAxis.axisMaximum = 30
        
        vwChart.legend.enabled = false
        
        let chartData: BarChartData = .init(dataSet: arrDataSet)
        chartData.barWidth = 0.6
        
        vwChart.setScaleEnabled(false)
        vwChart.data = chartData
        vwChart.minOffset = 0
        vwChart.notifyDataSetChanged()
        vwChart.setNeedsLayout()
    }
    
    private func findFastest() {
        var fastest = [String:Any]()
        let disGroup = DispatchGroup()
        neoData?.dictionaryValue.forEach { (key, value) in
            value.arrayValue.forEach { neo in
                if let approaches = neo.dictionaryValue["close_approach_data"]?.arrayValue, let neoId = neo.dictionaryValue["id"]?.stringValue, let name = neo.dictionaryValue["name"]?.stringValue {
                    approaches.forEach { approach in
                        disGroup.enter()
                        if let speed = approach.dictionaryValue["relative_velocity"]?.dictionaryValue["kilometers_per_hour"]?.doubleValue, (fastest["speed"] as? Double ?? 0) < speed {
                            fastest = [
                                "speed": speed,
                                "id": neoId,
                                "name": name,
                            ]
                        }
                        disGroup.leave()
                    }
                }
            }
        }
        
        disGroup.notify(queue: .main) {
            self.lblFastestAsteroid.text = String(format: "Fastest Among All is: %@(Id:%@) with %.2f km/h", (fastest["name"] as! String), (fastest["id"] as! String), (fastest["speed"] as! Double))
        }
    }
    
    private func findClosest() {
        var closest = [String:Any]()
        let disGroup = DispatchGroup()
        neoData?.dictionaryValue.forEach { (key, value) in
            value.arrayValue.forEach { neo in
                if let approaches = neo.dictionaryValue["close_approach_data"]?.arrayValue, let neoId = neo.dictionaryValue["id"]?.stringValue, let name = neo.dictionaryValue["name"]?.stringValue {
                    approaches.forEach { approach in
                        disGroup.enter()
                        if let distance = approach.dictionaryValue["miss_distance"]?.dictionaryValue["kilometers"]?.doubleValue, distance < (closest["distance"] as? Double ?? .infinity) {
                            closest = [
                                "distance": distance,
                                "id": neoId,
                                "name": name,
                            ]
                        }
                        disGroup.leave()
                    }
                }
            }
        }
        
        disGroup.notify(queue: .main) {
            self.lblClosestAsteroid.text = String(format: "Closest Among All is: %@(Id:%@) with %.2f km away", (closest["name"] as! String), (closest["id"] as! String), (closest["distance"] as! Double))
        }
    }
    
    
    private func findAverageSize() {
        var arrMax = [Double]()
        var arrMin = [Double]()
        let disGroup = DispatchGroup()
        neoData?.dictionaryValue.forEach { (key, value) in
            value.arrayValue.forEach { neo in
                disGroup.enter()
                if let size = neo.dictionaryValue["estimated_diameter"]?.dictionaryValue["kilometers"]?.dictionaryValue, let max = size["estimated_diameter_max"]?.doubleValue, let min = size["estimated_diameter_min"]?.doubleValue {
                    arrMax.append(max)
                    arrMin.append(min)
                }
                disGroup.leave()
            }
        }
        
        disGroup.notify(queue: .main) {
            self.lblAsteroidsAvgSize.text = String(format: "Avg Size of asteroid is: %.2f km to %.2f km", vDSP.mean(arrMin), vDSP.mean(arrMax))
        }
    }
    
    private func getAsteroidData(start: Date, end: Date) {
        var url = URLComponents(string: "https://api.nasa.gov/neo/rest/v1/feed")!
        url.queryItems = [
            URLQueryItem(name: "start_date", value: start.string()),
            URLQueryItem(name: "end_date", value: end.string()),
            URLQueryItem(name: "api_key", value: ApiKeys.nasa.rawValue),
        ]
        print(url)
        let task = URLSession.shared.dataTask(with: URLRequest(url: url.url!)) { data, response, error in
            self.HideHUD()
            if let error {
                print("API Failed With error: ",error.localizedDescription)
            } else if let response = response as? HTTPURLResponse, !(200...300).contains(response.statusCode) {
                print("API Failed With server error code: ",response.statusCode)
            } else if let data {
                let resp = try! JSON(data: data)
                self.neoData = resp.dictionaryValue["near_earth_objects"]
//                print("API Succeded With data: ",resp)
            } else {
                print("API Failed With Unknown error!")
            }
        }
        ShowHUD()
        task.resume()
    }
    
}

class CustomDateAxisFormatter: AxisValueFormatter {
    
    var arrData: [String]
    
    init(arrData: [String]) {
        self.arrData = arrData
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return "\(arrData[safe: Int(value)] ?? "-")"
    }
    
}
