//
//  Alert.swift
//  Asteroid NEO
//
//  Created by Nisarg Shah on 31/10/22.
//

import UIKit
import NVActivityIndicatorView
import NVActivityIndicatorViewExtended

extension UIViewController: NVActivityIndicatorViewable {
    
    func ShowHUD() {
        startAnimating(type: .ballClipRotatePulse, color: .systemMint, backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }
    
    func HideHUD() {
        stopAnimating()
    }
    
    func showAlert(
        title: String?,
        message: String?,
        buttonTitles: [String]? = nil,
        highlightedButtonIndex: Int? = nil,
        completion: ((Int) -> Void)? = nil) {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            var allButtons = buttonTitles ?? [String]()
            if allButtons.count == 0 {
                allButtons.append("OK")
            }
            for index in 0..<allButtons.count {
                let buttonTitle = allButtons[index]
                let action = UIAlertAction(title: buttonTitle, style: .default, handler: { _ in
                    completion?(index)
                })
                alertController.addAction(action)
                // Check which button to highlight
                if let highlightedButtonIndex = highlightedButtonIndex, index == highlightedButtonIndex {
                    alertController.preferredAction = action
                }
            }
            self.present(alertController, animated: true, completion: nil)
        }

}
