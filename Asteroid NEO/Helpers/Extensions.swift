//
//  Extensions.swift
//  Asteroid NEO
//
//  Created by Nisarg Shah on 31/10/22.
//

import Foundation

enum DateFormats: String {
    case yearMonthDate = "YYYY-MM-dd"
    case dateMonthYear = "dd-MM-YYYY"
}

extension Date {
    
    func string(withFormat format: DateFormats = .yearMonthDate) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        return dateFormatter.string(from: self)
    }
    
    @discardableResult func addingComp(_ unit: Calendar.Component, value: Int) -> Date {
        return Calendar.current.date(byAdding: unit, value: value, to: self) ?? Date()
    }
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        guard let start = Calendar.current.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = Calendar.current.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
    
}

extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension String {
    
    func toDate(withFormat format: String = "yyyy-MM-dd")-> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        return date
    }
    
}
