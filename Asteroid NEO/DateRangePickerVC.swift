//
//  DateRangePickerVC.swift
//  Asteroid NEO
//
//  Created by Nisarg Shah on 01/11/22.
//

import UIKit

class DateRangePickerVC: UIViewController {

    @IBOutlet private weak var txtStartDate: TextPickerView!
    @IBOutlet private weak var txtEndDate: TextPickerView!

    private var startDate: Date?
    private var endDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtStartDate.type = .date(self)
        txtEndDate.type = .date(self)
        txtStartDate.datePicker?.maximumDate = Date()
        txtEndDate.datePicker?.maximumDate = Date()
    }
    
    @IBAction private func btnFetchClicked(_ button: UIButton) {
        guard let startDate else {
            showAlert(title: "Validation Error!", message: "Please select start date!")
            return
        }
        guard let endDate else {
            showAlert(title: "Validation Error!", message: "Please select end date!")
            return
        }
        guard endDate.interval(ofComponent: .day, fromDate: startDate) <= 7 else {
            showAlert(title: "Validation Error!", message: "We can not fetch the data more then that of the week.")
            return
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "NearObjectChartVC") as! NearObjectChartVC
        vc.dateRange = (startDate,endDate)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension DateRangePickerVC: DatePickerViewDelegate {
    
    func textPickerView(_ textPickerView: TextPickerView, didSelectDate date: Date) {
        print("Scrolled through: ->", date)
        textPickerView.text = date.string()
        if textPickerView == txtStartDate {
            startDate = date
            txtEndDate.datePicker?.minimumDate = date
        } else {
            endDate = date
            txtStartDate.datePicker?.maximumDate = date
        }
    }
    
}
