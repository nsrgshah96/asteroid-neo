
import UIKit
import IQKeyboardManagerSwift

public enum TextPickerViewType {
    case date(DatePickerViewDelegate?), strings(TextPickerViewDelegate?)
}

@objc public protocol DatePickerViewDelegate {
    // date picker delegate
    @objc optional func textPickerView(_ textPickerView: TextPickerView, didSelectDate date: Date)
    @objc optional func textPickerView(_ textPickerView: TextPickerView, didClickedCanceled:Bool, date: Date)
}

@objc public protocol TextPickerViewDelegate {
    // strings picker delegate
    @objc optional func textPickerView(_ textPickerView: TextPickerView, didSelectString row: Int)
    @objc optional func textPickerView(_ textPickerView: TextPickerView, didClickedCanceled:Bool, row: Int)
    func textPickerView(_ textPickerView: TextPickerView, titleForRow row: Int) -> String?
    func numberOfRows(in textPickerView: TextPickerView) -> Int
}

public class TextPickerView: UITextField {
    
    // MARK: public properties
    public var type: TextPickerViewType = .strings(nil) {
        didSet {
            updatePickerType()
        }
    }
    
    // date picker properties
    public var currentDate = Date()
    private(set) public var datePicker: UIDatePicker?
    public var dateFormatter = DateFormatter()
    
    // strings picker properties
    private(set) public var dataPicker: UIPickerView?
    
    // MARK: init methods
    public override func awakeFromNib() {
        super.awakeFromNib()
        initialize()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
        addCancelDoneOnKeyboardWithTarget(self, cancelAction: #selector(cancelClicked(_:)), doneAction: #selector(doneClicked(_:)))
    }
    
    @objc func doneClicked(_ sender: UIBarButtonItem) {
        if endEditing(true) {
            switch type {
                case .strings(let delegate):
                    delegate?.textPickerView?(self, didClickedCanceled: false, row: dataPicker?.selectedRow(inComponent: 0) ?? 0)
                case .date(let delegate):
                    delegate?.textPickerView?(self, didClickedCanceled: false, date: datePicker?.date ?? Date())
            }
        }
    }
    
    @objc func cancelClicked(_ sender: UIBarButtonItem) {
        if endEditing(true) {
            switch type {
                case .strings(let delegate):
                    delegate?.textPickerView?(self, didClickedCanceled: true, row: dataPicker?.selectedRow(inComponent: 0) ?? 0)
                case .date(let delegate):
                    delegate?.textPickerView?(self, didClickedCanceled: true, date: datePicker?.date ?? Date())
            }
        }
    }
    
    private func initialize() {
        updatePickerType()
    }
    
    // MARK: private
    private func updatePickerType() {
        switch type {
            case .strings:
                initDataPicker()
            case .date:
                initDatePicker()
        }
    }
    
    // date picker setup
    private func initDatePicker() {
        dataPicker = nil
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker!.preferredDatePickerStyle = .wheels
        }
        datePicker?.backgroundColor = .white
        inputView = datePicker
        datePicker?.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
    }
    
    
    @objc func dateChanged(_ sender : UIDatePicker) {
        switch type {
            case .date(let delegate):
                delegate?.textPickerView?(self, didSelectDate: sender.date)
            default:
                break
        }
    }
    
    // strings data picker setup
    private func initDataPicker() {
        datePicker = nil
        dataPicker = UIPickerView()
        dataPicker?.delegate = self
        dataPicker?.dataSource = self
        dataPicker?.backgroundColor = .white
        inputView = dataPicker
    }
    
}

extension TextPickerView: UIPickerViewDataSource {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if case .strings(let delegate) = type {
            return delegate?.numberOfRows(in: self) ?? 0
        }
        return 0
    }
}

extension TextPickerView: UIPickerViewDelegate {
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if case .strings(let delegate) = type {
            return delegate?.textPickerView(self, titleForRow: row)
        }
        return nil
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if case .strings(let delegate) = type {
            delegate?.textPickerView?(self, didSelectString: row)
        }
    }
    
}
